# Web

![html](https://cdn.pixabay.com/photo/2015/12/04/14/05/code-1076536_960_720.jpg)

## Recherche d'informations

- Que trouve-t-on dans une balise `<head>` d'une page HTML ?

- A quoi correspondent les fichiers statiques ?

- Qu'est-ce que le DOM (Document Object Model) ?

- Qu'est-ce qu'un CDN (Content Delivery Network) ?

- A quoi servent les templates HTML de Flask ?

- Qu'est-ce que le Xpath ?

## Activité partie 1 : HTML

- Créer une application minimale avec Flask (https://flask.palletsprojects.com/en/2.1.x/quickstart/#a-minimal-application) sans utiliser de template (une seule vue) et vérifier son bon fonctionnement

- Créer un template de base en HTML et vérifier son bon fonctionnement (https://www.freecodecamp.org/news/basic-html5-template-boilerplate-code-example/)

- Ajouter les éléments suivants dans le template HTML :

    - Balise de titre avec le contenu "Veille technologique"

    - Liste non ordonnée avec un titre, une image (à partir d'une url) et un lien

## Activité partie 2 : CSS

- Ajouter un style CSS directement dans la balise H1

- Ajouter un style CSS pour les différentes balises H2 à partir d'une balise "style" dans la balise "head"

- Ajouter une feuille de style CSS externe au fichier HTML

- Vérifier les priorités de définition du CSS (interne ou externe)

## Activité partie 3 : librairie Bootstrap

- Ajouter les CDN de Bootstrap

- Mettre les élements de la liste dans des "cards" (https://getbootstrap.com/docs/4.0/components/card/)

- Ajouter des classes Bootstrap pour rendre responsible la page et vérifier le comportement responsive

## Activité partie 4 : Javascript

- Ajouter un formulaire pour ajouter un élément à la liste non ordonnée grace un code Javascript : le formulaire doit contenir 3 balises input et un bouton

- Mettre le code Javascript dans un fichier statique dans une fonction